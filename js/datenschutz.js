var datenschutz = document.getElementById("footerInfo").getElementsByTagName("a")[1];
var datenschutzOverlay = document.getElementsByClassName("datenschutzerklärung")[0];
var body = document.body;
var schließenKreuzDaten = document.getElementById("schließenKreuzDaten");


datenschutz.addEventListener("click", activeDatenschutz);

function activeDatenschutz(){
    datenschutzOverlay.classList.add("datenschutzOverlay");
    body.classList.add("noscroll");

}

schließenKreuzDaten.addEventListener("click", closeDatenschutz);


function closeDatenschutz(){
    datenschutzOverlay.classList.remove("datenschutzOverlay");
    body.classList.remove("noscroll");

}


