
var navUl = document.getElementsByTagName("header")[0].getElementsByTagName("ul")[0];

navUl.addEventListener("mouseover", function (a) {
    if (a.target && a.target.nodeName == "A") {
        hoverAnimationOver(a.target)
    }
});

navUl.addEventListener("mouseout", function (a) {
    if (a.target && a.target.nodeName == "A") {
        hoverAnimationOut(a.target)
    }
});

window.addEventListener("scroll", navbarAnimation)


let target1 = document.querySelectorAll('a[href^="#"]');
let navActive = document.getElementsByTagName("nav")[0].getElementsByTagName("span")[0];
var elemList = Array(target1.length - 1);
var elemPosList = Array(target1.length - 1);


for (j = 1; j < target1.length; j++) {
    let hash = target1[j].getAttribute("href");
    let elem = document.getElementById(hash.replace("#", ""));
    elemList[j - 1] = elem;
}

function hoverAnimationOver(a) {
    a.style.color = "#666666";
}

function hoverAnimationOut(a) {
    a.style.color = "#333333";
}


// Wenn Position im Window => Position des Anchors, dann färbe zugehöriges Nav
function navbarAnimation() {
    for (k = 1; k < target1.length; k++) {
        var headerHeight = 5 * document.documentElement.clientWidth / 100;
        elemPosList[k - 1] = parseInt(elemList[k - 1].offsetTop - headerHeight * 3);
    }
    let windowPosition = parseInt(window.pageYOffset);
    for (let index = 0; index < elemPosList.length; index++) {
        if ((elemPosList[index] <= windowPosition && elemPosList[index + 1] > windowPosition) || (elemPosList[index] <= windowPosition && index + 1 == elemPosList.length)) {
            var targetActive = target1[index + 1];

            for (l = 0; l < target1.length; l++) {
                if (l == index + 1) {

                    if(l==1){
                        navActive.style.opacity = "1"
                        navActive.style.transform = "none"
                    }

                    if (l == 2) {
                        navActive.style.opacity = "1"
                        navActive.style.transform = "translate(20.2vw)"
                    }

                    if (l == 3) {
                        navActive.style.opacity = "1"
                        navActive.style.transform = " translate(25.9vw,-1.3vw)  rotate(90deg) scaleY(3.5)"
                    }
                } 
            }

        }

    }

    if (elemPosList[0] > windowPosition) {
            navActive.style.transform = "none"
            navActive.style.opacity = "0"
    }

    if (windowPosition == 0) {
        navActive.style.display = "none"
    }

    else {
        navActive.style.display = "inline"
    }



}

