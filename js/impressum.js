var impressum = document.getElementById("footerInfo").getElementsByTagName("a")[0];
var impressumOverlay = document.getElementsByClassName("impressum")[0];
var body = document.body;
var schließenKreuz = document.getElementById("schließenKreuz");


impressum.addEventListener("click", activeImpressum);

function activeImpressum(){
    impressumOverlay.classList.add("impressumOverlay");
    body.classList.add("noscroll");

}

schließenKreuz.addEventListener("click", closeImpressum);


function closeImpressum(){
    impressumOverlay.classList.remove("impressumOverlay");
    body.classList.remove("noscroll");

}


