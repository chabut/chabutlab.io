var slideIndex = 0;
showSlides();


var stop;
var stop1;
var stop2;

function currentSlide(n) {
    clearTimeout(stop);
    clearTimeout(stop1);
    clearTimeout(stop2);
    showSlides(slideIndex = n - 1);
}


function showSlides() {
    var i;
    var slides = document.getElementsByClassName("slideShow");
    var dots = document.getElementsByClassName("dot");
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
        slides[i].style.opacity = 0;
    }

    slideIndex++;

    if (slideIndex > slides.length) { slideIndex = 1 }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }

    slides[slideIndex - 1].style.display = "block";

    stop1 = setTimeout(function () {
        slides[slideIndex - 1].style.opacity = 1;
    }, 600); //0.6 seconds after changing (transition duration of slideShow)

    dots[slideIndex - 1].className += " active";
    stop2 = setTimeout(function () {
        slides[slideIndex - 1].style.opacity = 0;
    }, 7400); // 0.6 seconds before changing (transition duration of slideShow)


    stop = setTimeout(showSlides, 8000); // Change image every 5 seconds

}