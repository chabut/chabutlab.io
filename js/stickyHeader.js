"use strict";

// When the user scrolls the page, execute myFunction
window.onscroll = function () { stickyHeader() };

// Get the header
var header = document.getElementsByTagName("header")[0];
var svgLogo = document.getElementsByTagName("header")[0].getElementsByTagName("svg")[0];
var headerLi = document.getElementsByTagName("header")[0].getElementsByTagName("ul")[0].getElementsByTagName("li");
var nav = document.getElementsByTagName("nav")[0];

// Get the offset position of the navbar
var sticky = header.offsetTop;

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function stickyHeader() {
    if (window.pageYOffset > sticky) {
        header.classList.add("headerSticky");
        svgLogo.classList.add("headerLogoSticky");
        for (let index = 0; index < headerLi.length; index++) {
            headerLi[index].classList.add("headerLiSticky");
        }
        nav.classList.add("headerNavSticky");

    } else {
        header.classList.remove("headerSticky");
        svgLogo.classList.remove("headerLogoSticky");
        for (let index = 0; index < headerLi.length; index++) {
            headerLi[index].classList.remove("headerLiSticky");
        }
        nav.classList.remove("headerNavSticky");
    }
}


let targ = document.querySelectorAll('a[href^="#"]');
let i = 0;

for (i = 0; i < targ.length; i++) {
    targ[i].onclick = function (e) {
        let hash = this.getAttribute("href");
        let elem = document.getElementById(hash.replace("#", ""));

        if (document.documentElement.clientWidth < 480) {
            var headerHeight = 10 * document.documentElement.clientWidth / 100;
        }
        else {
            var headerHeight = 5 * document.documentElement.clientWidth / 100;
        }
        var pos = elem.offsetTop - headerHeight;
        window.scrollTo({ top: pos, left: 0, behavior: "smooth" });
        e.preventDefault();
    }
}