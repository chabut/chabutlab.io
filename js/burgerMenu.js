var burger = document.getElementById("burger")
var navUl = document.getElementsByTagName("nav")[0].getElementsByTagName("ul")[0];
var navUlLi = document.getElementsByTagName("nav")[0].getElementsByTagName("ul")[0].getElementsByTagName("li");
var navLogo = document.getElementsByTagName("header")[0].getElementsByTagName("svg")[0];


burger.addEventListener("click", activeBurger)

function activeBurger() {

    burger.classList.toggle("activeBurger");
    navUl.classList.toggle("navOverlay");
};


for (x = 0; x < navUlLi.length; x++) {

    navUlLi[x].addEventListener("click", closeOverlay)

};

navLogo.addEventListener("click", closeOverlay)

function closeOverlay () {

    burger.classList.remove("activeBurger");
    navUl.classList.remove("navOverlay");

};

